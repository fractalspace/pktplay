//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by pktPlay.rc
//
#define IDD_DIALOG2                     102
#define IDR_GIF1                        109
#define IDR_GIF2                        110
#define IDB_BITMAP1                     112
#define IDB_BITMAP2                     118
#define IDB_BITMAP3                     119
#define IDI_ICON1                       120
#define IDD_DIALOG1                     126
#define IDB_BITMAP4                     130
#define IDC_EDIT1                       1002
#define IDC_LIST1                       1015
#define IDC_FILENAME                    1016
#define IDC_BROWSE                      1021
#define IDC_BTN_TEST                    1025
#define IDC_STATIC4                     1032
#define IDC_BUTTON1                     1036
#define IDC_ABOUT                       1036
#define IDOK1                           1038
#define IDC_SPEED_MAX                   1039
#define IDC_SPEED_CAP                   1040
#define IDC_EDIT3                       1043
#define IDC_LOOPS                       1044
#define IDC_STATUS                      1045
#define IDC_LIST2                       1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1050
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
