/*----------------------------------------------------------------*\
* A simple tool to play captured packets from a file.
* ----------------------------------------------------
* Uses WinPcap library.
* (c) 2007 Shahid Mahmood <fractalspace@gmail.com>
*
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
\*----------------------------------------------------------------*/

// TODO: Add progress bar, picture, speed option, About.
// Interrupt

#define HAVE_REMOTE
#include "pcap.h"
#include <stdio.h>
#include <windows.h>
#include <io.h>
#define WIN32_LEAN_AND_MEAN   // Quicker build times;

#include "resource.h"
#include "commctrl.h"
#define WINDOWNAME "PktPlay - Replay .cap File to network"

typedef int (*intfv) (pcap_t *);

pcap_if_t *alldevs;
int nEthDevs;
char szFileName[MAX_PATH+1] = {'\0'};
int ifSelectedIdx=-1;

/* handles to controls */
HWND hList,hSend,hFileName,hPicBox,hStatus,hPrgrs,hPrgMax;
HANDLE hSendThread=NULL;

typedef struct THREAD_PARAMS {
	HWND hwnd;// handle of dialog to send status back
	pcap_if_t * curdrv;
	char * pktFile;
	bool maxSpeed;
	int loops;
} THREAD_PARAMS ;

THREAD_PARAMS thParams;


/* ---- fwd dec ----*/
LRESULT ProcessCustomDraw (LPARAM lParam);
int packet_send_pkt(pcap_if_t * curdrv, char * pktFile, bool maxSpeed, int loops);
DWORD * WINAPI PktSendThread(LPVOID *params);
int GetTicksPerMs(void);
void UpdateStatus(char * msg);

/**
*  Open browse dialog and let user select a file
*/
int GetFileName(HWND hwnd)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = "Ethereal Capture Files (*.cap,*.pcap)\0*.cap\0*.pcap\0All Files (*.*)\0*.*\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = "cap";
	return GetOpenFileName(&ofn)?0:-1;
}

/* ---- About Dialog ---- */
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch(Message)
	{
	case WM_COMMAND:
		switch(LOWORD(wParam)){
			case IDOK1:
			EndDialog(hwnd,0); // kill dialog
			DestroyWindow(hwnd);
			return true;
			break;
		}
	case WM_CLOSE:
	case WM_DESTROY:
		EndDialog(hwnd,0); // kill dialog
		DestroyWindow(hwnd);
		return false;
		break;
	default:
		return false;
	}
	return true;
}

/*----------------------------------------------------------------*\
Global Variables - to vanish in the C++ version;
\*----------------------------------------------------------------*/

BOOL CALLBACK ToolDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	//char buf[200]={"Hello World"};
	pcap_if_t * curdrv=alldevs;
	HWND hAbout;
	BOOL translated;

	//		SendDlgItemMessage(hWdlg,IDOK,WM_ENABLE,1,0);

//	MSG msg;
	switch(Message)
	{
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_LIST1:
			//MessageBox(hwnd, "Radio2!", "This is also a IDC_RADIO2", MB_OK | MB_ICONEXCLAMATION);
			ifSelectedIdx = (int)SendDlgItemMessage(hwnd, IDC_LIST1, LB_GETCURSEL,0,0);
			if ( ifSelectedIdx>=0 && ifSelectedIdx<nEthDevs) {
				for (int i=0;i<ifSelectedIdx;i++)
					curdrv=curdrv->next;
				if (curdrv) SendDlgItemMessage(hwnd,IDC_EDIT1,WM_SETTEXT,0,(LPARAM)curdrv->name);
				else SendDlgItemMessage(hwnd,IDC_EDIT1,WM_SETTEXT,0,(LPARAM)"Invalid Device!");
			}
			else SendDlgItemMessage(hwnd,IDC_EDIT1,WM_SETTEXT,0,(LPARAM)"Index Error ");
			//			if ((ifSelectedIdx>=0) && (strlen(szFileName)>0))
			//				SendDlgItemMessage(hwnd,IDOK,BN_DISABLE,0x1111,0);
			break;
		case IDC_FILENAME:
			GetDlgItemText(hwnd,IDC_FILENAME,szFileName,MAX_PATH);
			break;
		case IDOK:
			if ((ifSelectedIdx<0) || (strlen(szFileName)==0))
				MessageBox(hwnd,"Please select a file AND an Adaptor","Incomplete Selection",MB_OK|MB_ICONEXCLAMATION);
			else {
				if ( ifSelectedIdx>=0 && ifSelectedIdx<nEthDevs) {
					int loops=GetDlgItemInt(hwnd,IDC_LOOPS,&translated,true);
					for (int i=0;i<ifSelectedIdx;i++)
						curdrv=curdrv->next;
					if (curdrv){
						memset(&thParams,0,sizeof(THREAD_PARAMS));
						thParams.hwnd=hwnd;
						thParams.curdrv=curdrv;
						thParams.maxSpeed=true;
						thParams.pktFile=szFileName;
						thParams.loops=translated?(loops>0?loops:0):0;
						hSendThread=CreateThread(0,0,(LPTHREAD_START_ROUTINE)PktSendThread,&thParams,0,0);
						}
					}
					else
						MessageBox(hwnd,"Invalid Device selected","Selection Incomplete",MB_OK|MB_ICONEXCLAMATION);
				}
			break;
		case IDCANCEL:
			if (hSendThread)TerminateThread(PktSendThread,0);
			WaitForSingleObject(hSendThread,INFINITE);
			CloseHandle(hSendThread);
			//DestroyWindow(hwnd);
			PostQuitMessage(0);
			return false;
			break;
		case IDC_BROWSE:
			GetFileName(hwnd);
			SendDlgItemMessage(hwnd,IDC_FILENAME,WM_SETTEXT,0,(LPARAM)szFileName);
			break;
		case IDC_ABOUT:
			hAbout = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1),hwnd,AboutDlgProc);
			ShowWindow(hAbout,SW_SHOW);
			break;
		case IDC_SPEED_MAX:
		case IDC_SPEED_CAP:
			CheckRadioButton(hwnd,IDC_SPEED_MAX,IDC_SPEED_CAP,LOWORD(wParam));
			break;
		case IDC_LOOPS:
			if (GetDlgItemInt(hwnd,IDC_LOOPS,&translated,true)>256)
					SendDlgItemMessage(hwnd,IDC_LOOPS,WM_SETTEXT,0,(LPARAM)"256"); //max 256
			if (GetDlgItemInt(hwnd,IDC_LOOPS,&translated,true)<=0)
				SendDlgItemMessage(hwnd,IDC_LOOPS,WM_SETTEXT,0,(LPARAM)"1"); //min 1

			break;
		}
		break;
	case WM_CLOSE:
	case WM_DESTROY:
		UpdateStatus("Exiting ...");
		if (hSendThread) TerminateThread(hSendThread,0);
		WaitForSingleObject(hSendThread,INFINITE);
		//if (GetThreadId(hSendThread))TerminateThread(PktSendThread,0);
		CloseHandle(hSendThread);
		//EndDialog(hwnd,0); // kill dialog
		DestroyWindow(hwnd);
		PostQuitMessage(0);
		return false;
		break;
	default:
		//SendDlgItemMessage(hwnd,IDC_STATUS,WM_SETTEXT,0,(LPARAM)(hSendThread?"Sending!":"Idle"));
		return false;
	}
	return true;
}
/*----------------------------------------------------------------*\
Function: WinMain();
Purpose:  Program initial entry point;
\*----------------------------------------------------------------*/

int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrev, LPSTR lpCmdLine, int Cmd) {

	// Dialog Box
	HWND hWdlg = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG2),0,ToolDlgProc);
	if (hWdlg==NULL) {
		MessageBox(NULL,"Error creating dialog box","Error",MB_OK|MB_ICONERROR);
		return -1;
	}
	INITCOMMONCONTROLSEX comCtrl;
	InitCommonControlsEx(&comCtrl);
		
    hList=GetDlgItem(hWdlg,IDC_LIST1);
	hSend=GetDlgItem(hWdlg,IDOK);
	hFileName=GetDlgItem(hWdlg,IDC_FILENAME);
	hStatus=GetDlgItem(hWdlg,IDC_STATUS);
	hPrgMax=GetDlgItem(hWdlg,IDC_STATIC4);

	SendMessage(hWdlg,WM_SETTEXT,0,(LPARAM)WINDOWNAME);
	SendMessage(hPicBox,WM_SETICON,0,(LPARAM)IDI_ICON1);

	//SendMessage(GetDlgItem(hWdlg,IDC_LOOPS),WM_SETTEXT,0,(LPARAM)0);
	SendDlgItemMessage(hWdlg,IDC_FILENAME,WM_SETTEXT,0,(LPARAM)szFileName);
	CheckRadioButton(hWdlg,IDC_SPEED_MAX,IDC_SPEED_CAP,IDC_SPEED_MAX);
	SendDlgItemMessage(hWdlg,IDC_LOOPS,WM_SETTEXT,0,(LPARAM)"1");
	SendDlgItemMessage(hWdlg,IDC_EDIT1,WM_SETTEXT,0,(LPARAM)"Select an Ethernet Adaptor");
	SendDlgItemMessage(hWdlg,IDC_EDIT1,WM_SETFOCUS,0,0);
	UpdateStatus("Waiting for user input.");

	ShowWindow(hWdlg, SW_SHOW);

	// Fill combo box with driver names.
	char errbuf[PCAP_ERRBUF_SIZE];

	/* Retrieve the device list */
	if(pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		sprintf_s(errbuf,100,"Error Reading Ethernet Driver List:\n %s", errbuf);
		MessageBox(0,errbuf,"Error",MB_OK|MB_ICONERROR);
		return -1;
	}

	pcap_if_t *curdrv=alldevs;
	int count=0;
	
	SendMessage(hSend,WM_ENABLE,0,0);

	while ( curdrv!=NULL ) {
		SendDlgItemMessage(hWdlg, IDC_LIST1, LB_ADDSTRING,0,(LPARAM)curdrv->description);
		//SendMessage(hList, LVM_INSERTITEM,0,(LPARAM)&LvItem); // Enter text to SubItems
		SendMessage(hList, WM_ENABLE,0,0);
		curdrv=curdrv->next;
		count++;
	}
	nEthDevs=count;

	// Message loop;
	MSG msg;
	while(GetMessage(&msg,NULL,0,0)>0) {
		if(!IsDialogMessage(hWdlg, &msg)){
			TranslateMessage(&msg);
			DispatchMessage (&msg);
		}
	}

	return (int)msg.wParam;
}
void UpdateStatus(char * msg)
{
	SendMessage(hStatus,LB_ADDSTRING,0,(LPARAM)msg);
	//SendMessage(hStatus,WM_ENABLE,0,0);
	SendMessage(hStatus,EM_SCROLL,(WPARAM)SB_LINEDOWN,0);
	SendMessage(hStatus,LB_SETCURSEL,(WPARAM)1,0);
}
int GetTicksPerMs(void)
{
	int i=0;
	int tick_was;
	tick_was=GetTickCount();
	Sleep(1);// 1ms
	return (GetTickCount()-tick_was);
}
/* -- thread that launches the packet_send_pkt process --*/
DWORD * WINAPI PktSendThread(LPVOID *params)
{
	THREAD_PARAMS * tp = (THREAD_PARAMS*)params;
	UpdateStatus("Sending...");
	packet_send_pkt(tp->curdrv,tp->pktFile,tp->maxSpeed,tp->loops);
	UpdateStatus("Finished Sending.");
	UpdateStatus("Idle.");
	hSendThread=NULL;
	ExitThread(0);
}

int packet_send_pkt(pcap_if_t * ifdev, char * pktFile,  bool maxSpeed, int loops)
{
	pcap_t *fp;
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *fpFile;
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
	u_int i=0;
	int res;
	int tickPerMicroSec=GetTicksPerMs()/1000;

	/* Open the adapter */
	if ((fp = pcap_open_live(ifdev->name,65536,	1,1000,	errbuf)) == NULL)
	{
		sprintf_s(errbuf,100,"\nUnable to open the adapter. %s is not supported by WinPcap\n",
			ifdev->description);
		MessageBox(0,errbuf,"Error",MB_OK|MB_ICONERROR);
		return 2;
	}

	/* Open the capture file */
	if ((fpFile = pcap_open_offline(pktFile, errbuf)) == NULL)
	{
		sprintf_s(errbuf,100,"\nUnable to open the file: \n\"%s.\"\n", pktFile);
		MessageBox(0,errbuf,"Error",MB_OK|MB_ICONERROR);
		return -1;
	}

	//pcap_snapshot 
//	HANDLE hFile = CreateFile(pktFile, GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
//	int fileSize = GetFileSize(hFile,NULL);
//	CloseHandle(hFile);
	/* Retrieve the packets from the file */
	int pkt_total=0;

	// compute sum of all packet sizes
	while ((res = pcap_next_ex(fpFile, &header, &pkt_data)) >= 0) {
		pkt_total++;
	}
	if (fpFile) pcap_close(fpFile);

	// re-open	
do {// loops
	if ((fpFile = pcap_open_offline(pktFile, errbuf)) == NULL)
	{
		sprintf_s(errbuf,100,"\nUnable to open the file: \n\"%s.\"\n", pktFile);
		MessageBox(0,errbuf,"Error",MB_OK|MB_ICONERROR);
		return -1;
	}
	
	while((res = pcap_next_ex(fpFile, &header, &pkt_data)) >= 0)
	{
		/* Send down the packet */
		if (pcap_sendpacket(fp,	pkt_data,header->len) != 0)
		{
			sprintf_s(errbuf,250,"\nError sending the packet to Adaptor: \n\"%s\"",ifdev->description );
			MessageBox(0,errbuf,"Error",MB_OK|MB_ICONERROR);
			return 3;
		}
	}
	pcap_close(fpFile);
	fpFile=NULL;
	if (!maxSpeed) { // delay based on packet delay
		// 1 sec delay
		unsigned int delay1s=header->ts.tv_sec;
		while (delay1s--) Sleep(1000);
		// micro second delay
		unsigned int tickStart=GetTickCount();
		unsigned int tickEnd=tickStart+header->ts.tv_usec;
		while(GetTickCount()<tickEnd)
					_asm{nop};
		
	}
	} while (--loops); // 0=1 time

	if (fp) pcap_close(fp);
	if (fpFile) pcap_close(fpFile);
	return 0;
}
